package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.todo_item.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var todoAdapter: TodoAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvTodoItem.adapter = todoAdapter
        rvTodoItem.layoutManager = LinearLayoutManager(this)

        salvarButton.setOnClickListener {
            val todoTittle = taskTextView?.text.toString()
            if (todoTittle.isNotEmpty()) {
                val todo = Todo(todoTittle)
                todoAdapter?.addTodo(todo)
            }
        }

        limparButton.setOnClickListener {
            todoAdapter?.deleteDoneTodos()
        }
    }
}